class Fixnum
  SINGLES_IDX = [0, 3, 6, 9, 12, 15].freeze
  TENS_IDX = [1, 4, 7, 10, 13].freeze
  HUNDREDS_IDX = [2, 5, 8, 11, 14].freeze
  SINGLE_ARR = %w[one two three four five six seven eight nine].freeze
  TEENS = %w[ten eleven twelve thirteen fourteen fifteen
             sixteen seventeen eighteen nineteen].freeze
  TENS_DIGIT = %w[twenty thirty forty fifty sixty seventy eighty ninety].freeze
  LARGE_NUMS = ['', 'thousand', 'million', 'billion', 'trillion'].freeze

  def in_words
    result = ''
    return 'zero' if self.zero?
    worded = reorderer(word_processor(self.to_s.reverse))
    worded.each_with_index do |arr, idx|
      counter = worded.size - idx - 1
      words = strip_space(arr)
      words == '' ? next : result << "#{words} #{LARGE_NUMS[counter]} "
    end
    result.strip
  end

  def strip_space(arr)
    arr.join(' ').strip
  end

  def word_processor(str)
    result = []
    str.each_char.with_index do |digit, idx|
      next_digit = str[idx + 1]
      num = str[idx - 1..idx]
      result << num_evaluator(idx, digit, num, next_digit)
    end
    result
  end

  def reorderer(arr)
    arr.each_slice(3).to_a.map(&:reverse).reverse
  end

  def num_evaluator(idx, digit, num, next_digit)
    if SINGLES_IDX.include?(idx) && next_digit != '1'
      single_processor(digit)
    elsif TENS_IDX.include?(idx)
      tens_processor(num, digit)
    elsif HUNDREDS_IDX.include?(idx) && digit != '0'
      single_processor(digit) + ' hundred'
    end
  end

  def single_processor(digit)
    digit == '0' ? '' : SINGLE_ARR[digit.to_i - 1]
  end

  def tens_processor(num, digit)
    return '' if digit == '0'
    digit == '1' ? TEENS[num.to_i / 10] : TENS_DIGIT[digit.to_i - 2]
  end
end
